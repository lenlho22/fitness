export enum Sex {
    MALE = 'm',
    FEMALE = 'f',
}

export type ValueType = number | string | boolean;

export interface IProfile {
    fname: string;
    lname: string;
    email: string;
    password: string;
    age: number;
    sex: string;
    height: number;
    weight: number;
}


export interface IRecord {
    type: string;
    record: ValueType;
}

export interface IResponseRecord {
    hash: string;
    value: ValueType;
}

export interface ICredentialsModel {
    email: string;
    password: string;
}
export interface IToken {
    data: string;
}

export type ErrorType = {
    response: {
        data: {
            statusCode: number,
            message:  string,
            error:    string,
        }
    }
};

export type InputInfoType = {
    name: keyof IProfile,
    type: string,
    label: string,
    placeholder: string,
};
