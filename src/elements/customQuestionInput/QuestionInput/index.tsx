import { FC, useEffect, useState } from 'react';
import { observer } from 'mobx-react-lite';

import { useGetRecord, useCreateRecord, useUpdateRecord } from 'hooks';

import styles from '../styles/index.module.scss';

interface IQuestionInputProps {
    type: string;
    title: string;
    placeholder: string;
}

export const QuestionInput:FC<IQuestionInputProps> = observer(({ type, title, placeholder }) => {
    const [value, setValue] = useState<number | string | undefined>('');

    const { data, isFetched } = useGetRecord(type);
    const createRecord = useCreateRecord();
    const updateRecord = useUpdateRecord();

    useEffect(() => {
        if (isFetched && data?.value && typeof data?.value !== 'boolean') {
            setValue(data.value);
        }
    }, [isFetched, data]);

    const handleSubmit = async () => {
        if (!value) return;
        const record = {
            type,
            record: value,
        };

        if (data?.hash && data?.hash !== '0') {
            await updateRecord.mutateAsync({ record, hash: data.hash });
        } else {
            await createRecord.mutateAsync(record);
        }
    };

    return (
        <div className = { styles.question }>
            <h1>{ title }</h1>
            <div className = { styles.inputRow }>
                <input
                    type = 'number'
                    placeholder = { placeholder }
                    value = { value }
                    onChange = { ({ target }) => setValue(target.value) } />

            </div>

            <button
                disabled = { !value }
                className = { styles.sendAnswer }
                onClick = { handleSubmit }>
                Ответить
            </button>
        </div>
    );
});
