import { FC } from 'react';

import styles from 'bus/user/components/profile/styles/index.module.scss';

interface IErrorMessage {
    message: string;
}

export const ErrorMessage: FC<IErrorMessage> = ({ message }) => {
    return (
        <p className = { styles.error }>{ message }</p>
    );
};
