import { FC, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { Link } from 'react-router-dom';
import cx from 'classnames';

import { useStore, useAuth, useGetUser } from 'hooks';
import { Sex } from 'types';

import styles from '../styles/index.module.scss';

export const ProfileAvatar:FC = observer(() => {
    const { userStore, uiStore } = useStore();
    const { logout } = useAuth();
    const { data:user, isLoading, isFetched } = useGetUser();

    const sex = userStore.getSex();

    useEffect(() => {
        uiStore.setIsLoading(isLoading);
        if (isFetched && user) {
            // @ts-ignore
            const { hash, ...newUser } = user;

            userStore.setUser(newUser);
        }
    }, [isFetched, isLoading]);

    return (
        <div
            className = { cx(styles.avatar, {
                [ styles.male ]:   sex === Sex.MALE,
                [ styles.female ]: sex === Sex.FEMALE,
            }) }>
            <div className = { styles.column }>
                <Link
                    className = { styles.name }
                    to = '/profile'>
                    { userStore.getUserName() }
                </Link>
                <button className = { styles.logout } onClick = { logout }>Выйти</button>
            </div>
        </div>
    );
});
