// Components
import {
    DashboardPage,
    LoginPage,
    RegistrationPage,
    BreakfastPage,
    DinnerPage,
    LunchPage,
    CoffeePage,
    FruitsPage,
    VegetablesPage,
    JunkPage,
    WaterPage,
    StepsPage,
    SleepPage,
    ProfilePage,
} from 'pages';

const base = '/';

export const book = Object.freeze({
    root: {
        url:  `${base}`,
        page: DashboardPage,
    },
    login: {
        url:  `${base}login`,
        page: LoginPage,
    },
    registration: {
        url:  `${base}registration`,
        page: RegistrationPage,
    },
    profile: {
        url:  `${base}profile`,
        page: ProfilePage,
    },
    breakfast: {
        url:  `${base}breakfast`,
        page: BreakfastPage,
    },
    coffee: {
        url:  `${base}coffee`,
        page: CoffeePage,
    },
    dinner: {
        url:  `${base}dinner`,
        page: DinnerPage,
    },
    fruits: {
        url:  `${base}fruits`,
        page: FruitsPage,
    },
    junk: {
        url:  `${base}junk`,
        page: JunkPage,
    },
    lunch: {
        url:  `${base}lunch`,
        page: LunchPage,
    },
    sleep: {
        url:  `${base}sleep`,
        page: SleepPage,
    },
    steps: {
        url:  `${base}steps`,
        page: StepsPage,
    },
    vegetables: {
        url:  `${base}vegetables`,
        page: VegetablesPage,
    },
    water: {
        url:  `${base}water`,
        page: WaterPage,
    },
});

export type BookType = typeof book;
