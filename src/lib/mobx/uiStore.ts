// Core
import { makeAutoObservable } from 'mobx';

export class UiStore {
    isLoading: boolean;
    errorMessage: string;

    constructor() {
        this.isLoading = false;
        this.errorMessage = '';

        makeAutoObservable(this, {}, { autoBind: true });
    }

    setIsLoading(isLoading: boolean) {
        this.isLoading = isLoading;
    }

    setErrorMessage(message:string) {
        this.errorMessage = message;
    }
}
