// Core
import { makeAutoObservable } from 'mobx';

// Types
import { RootStore } from '.';

const MAX_SCORE = 100;

export class TrackerStore {
    root: RootStore;
    score: number;

    constructor(root: RootStore) {
        this.root = root;
        this.score = 0;

        makeAutoObservable(this, {}, { autoBind: true });
    }

    setScore(score: number) {
        this.score = score > MAX_SCORE ? MAX_SCORE : score;
    }
}
