// Core
import { makeAutoObservable } from 'mobx';

// Types
import { IProfile } from 'types';
import { RootStore } from '.';

export class UserStore {
    root: RootStore;
    token: string | null;
    user: IProfile | null;

    constructor(root: RootStore) {
        this.root = root;
        this.token = null;
        this.user = null;

        makeAutoObservable(this, {}, { autoBind: true });
    }

    setToken(token:string | null) {
        this.token = token;
    }

    setUser(user:IProfile | null) {
        if (user) {
            this.user = { ...user };
        } else {
            this.token = null;
            this.user = null;
        }
    }

    getUserName() {
        if (this.user?.fname || this.user?.lname) {
            return `${this.user?.fname} ${this.user?.lname}`;
        }

        return 'Anonymous';
    }

    getSex() {
        return this.user?.sex;
    }
}
