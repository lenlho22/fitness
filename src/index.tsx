// Core
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { configure } from 'mobx';
import { QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';

// Components
import { queryClient } from 'lib/react-query';
import { Provider } from 'lib/Provider';

// Instruments
import { RoutesComponent } from 'navigation';

// Styles
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import 'theme/index.scss';

configure({
    enforceActions:             'always',
    computedRequiresReaction:   true,
    observableRequiresReaction: true,
    reactionRequiresObservable: true,
});

render(
    <Provider>
        <QueryClientProvider client = { queryClient }>
            <Router>
                <RoutesComponent />
            </Router>
            <ReactQueryDevtools initialIsOpen = { false } />
        </QueryClientProvider>
    </Provider>,
    document.getElementById('root'),
);
