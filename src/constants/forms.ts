/* eslint-disable no-template-curly-in-string */
export const tooShortMessage = 'Минимальная длина — ${min} символов';
export const tooLongMessage = 'Максимальная длина — ${max} символов';
export const wrongEmailMessage = 'Почта должна быть настоящей';
export const requiredEmail = 'Поле email обязательно для заполнения';
export const requiredSex = 'Поле пол обязательно для заполнения';
export const tooSmallValue = 'Минимальная величина должна быть — ${min}';
export const shouldBeNumber = 'Значение должно быть числом';

export const PASSWORD_MIN_LENGTH = 8;
export const PASSWORD_MAX_LENGTH = 64;
export const TEXT_MIN_LENGTH = 2;
export const TEXT_MAX_LENGTH = 64;
