import { InputInfoType } from 'types';

export const profileInputs: InputInfoType[] = [
    {
        name:        'email',
        type:        'email',
        label:       'Электропочта',
        placeholder: 'Введите свой email',
    },
    {
        name:        'fname',
        type:        'text',
        label:       'Имя',
        placeholder: 'Введите свое имя',
    },
    {
        name:        'lname',
        type:        'text',
        label:       'Фамилия',
        placeholder: 'Введите свою фамилию',
    },
    {
        name:        'password',
        type:        'password',
        label:       'Пароль',
        placeholder: 'Введите свой пароль',
    },
    {
        name:        'age',
        type:        'number',
        label:       'Возраст',
        placeholder: 'Введите свой возраст',
    },
    {
        name:        'height',
        type:        'number',
        label:       'Рост',
        placeholder: 'Введите свой рост',
    },
    {
        name:        'weight',
        type:        'number',
        label:       'Вес',
        placeholder: 'Введите свой вес',
    },
];
