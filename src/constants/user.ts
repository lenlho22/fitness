export const defaultUser = {
    fname:    '',
    lname:    '',
    email:    '',
    password: '',
    age:      NaN,
    sex:      '',
    height:   NaN,
    weight:   NaN,
};
