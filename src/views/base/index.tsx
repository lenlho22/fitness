// Core
import { FC, ReactElement, useEffect } from 'react';
import cx from 'classnames';
import {
    Link, matchPath, useLocation,
} from 'react-router-dom';
import { observer } from 'mobx-react-lite';

// Book
import { book } from 'navigation/book';

// Elements
import { Spinner } from 'elements/spinner';

// Hooks
import { useStore, useGetScore } from 'hooks';
import { Sex } from 'types';

// Components
import { ProfileAvatar } from 'elements/user';

// Styles
import styles from './styles/index.module.scss';

export const Base: FC<IPropTypes> = observer((props) => {
    const { pathname } = useLocation();
    const { uiStore, userStore, trackerStore } = useStore();
    const { score } = trackerStore;
    const { isLoading, errorMessage, setErrorMessage } = uiStore;
    const { data, isFetched } = useGetScore();

    const sex = userStore.getSex();

    useEffect(() => {
        if (isFetched && data !== undefined) {
            trackerStore.setScore(data);
        }
    }, [isFetched, data]);

    const {
        children,
        center,
        disabledWidget,
    } = props;

    const isExact = matchPath(book.root.url, pathname);

    const avatarCX = cx(
        styles.sidebar, {
            [ styles.male ]:   sex === Sex.MALE,
            [ styles.female ]: sex === Sex.FEMALE,
        },
    );

    const contentCX = cx(styles.content, {
        [ styles.center ]: center,
    });

    const loaderCX = isLoading && (
        <Spinner isLoading = { isLoading } />
    );

    const widgetJSX = score !== null && !disabledWidget && (
        <div className = { styles.widget }>
            <span className = { styles.title }>Life Score</span>
            <div className = { styles.module }>
                <span className = { styles.score } style = { { bottom: `${score}%` } }>{ score }</span>
                <div className = { styles.progress }>
                    <div className = { styles.fill } style = { { height: `${score}%` } } />
                </div>
                <span className = { cx(styles.label, styles.level1) }>Off Track</span>
                <span className = { cx(styles.label, styles.level2) }>Imbalanced</span>
                <span className = { cx(styles.label, styles.level3) }>Balanced</span>
                <span className = { cx(styles.label, styles.level4) }>Healthy</span>
                <span className = { cx(styles.label, styles.level5) }>Perfect Fit</span>
            </div>
        </div>
    );

    const homeLinkJSX = !isExact && (
        <Link to = { book.root.url } className = { styles.homeLink }>На главную</Link>
    );

    return (
        <section className = { styles.profile }>
            <div className = { avatarCX }>
                { loaderCX }
            </div>
            <div className = { styles.wrap }>
                <div className = { styles.header }>
                    <div>
                        { homeLinkJSX }
                    </div>
                    <ProfileAvatar />
                </div>
                <div className = { contentCX }>
                    { children }
                    { widgetJSX }
                </div>
            </div>
            { errorMessage && <p className = { styles.errorMessage }>
                { errorMessage }
                <button className = { styles.clearButton } onClick = { () => setErrorMessage('') }>x</button>
            </p> }
        </section>
    );
});

interface IPropTypes {
    children: ReactElement | ReactElement[];
    center?: boolean;
    disabledWidget?: boolean;
}
