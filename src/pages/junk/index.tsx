import { FC } from 'react';

import { useCleanPrevError } from 'hooks';

import { Base } from 'views/base';

import { QuestionSlector } from 'elements/customQuestionSelector';

const junk = {
    type:   'junk',
    title:  'Ты сегодня кушал Фастфуд?',
    inputs: [
        {
            id:    1,
            label: 'Да',
            value: true,
        },
        {
            id:    2,
            label: 'Нет',
            value: false,
        },
    ],
};

export const JunkPage:FC = () => {
    useCleanPrevError();

    return (
        <Base>
            <QuestionSlector  { ...junk } />
        </Base>
    );
};
