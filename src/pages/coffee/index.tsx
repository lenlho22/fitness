import { FC } from 'react';

import { useCleanPrevError } from 'hooks';

import { Base } from 'views/base';

import { QuestionSlector } from 'elements/customQuestionSelector';

const coffee = {
    type:   'coffee',
    title:  'Ты сегодня пил кофе?',
    inputs: [
        {
            id:    1,
            label: 'Я не пил совсем',
            value: 'none',
        },
        {
            id:    2,
            label: 'Выпил 1 стакан',
            value: 'light',
        },
        {
            id:    3,
            label: 'Выпил 2 или больше стаканов',
            value: 'heavy',
        },
    ],
};

export const CoffeePage:FC = () => {
    useCleanPrevError();

    return (
        <Base>
            <QuestionSlector { ...coffee } />
        </Base>
    );
};
