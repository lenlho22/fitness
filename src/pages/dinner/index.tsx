import { FC } from 'react';

import { useCleanPrevError } from 'hooks';

import { Base } from 'views/base';

import { QuestionSlector } from 'elements/customQuestionSelector';

const dinner = {
    type:   'dinner',
    title:  'Ты сегодня ужинал?',
    inputs: [
        {
            id:    1,
            label: 'Я не ужинал',
            value: 'none',
        },
        {
            id:    2,
            label: 'У меня был легкий ужин',
            value: 'light',
        },
        {
            id:    3,
            label: 'Я очень плотно покушал',
            value: 'heavy',
        },
    ],
};

export const DinnerPage:FC = () => {
    useCleanPrevError();

    return (
        <Base>
            <QuestionSlector { ...dinner } />
        </Base>
    );
};
