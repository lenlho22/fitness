// Core
import { FC } from 'react';

import { useCleanPrevError } from 'hooks';

// Views
import { ProfileForm } from 'bus/user/components/profile';
import { defaultUser } from 'constants/user';

import styles from 'bus/user/components/registration/styles/index.module.scss';

export const RegistrationPage:FC = () => {
    useCleanPrevError();

    return (
        <section className = { styles.registration }>
            <div className = { styles.left }>
                <ProfileForm registration { ...defaultUser } />
            </div>
            <div className = { styles.right } />
        </section>
    );
};
