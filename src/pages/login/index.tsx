// Core
import { FC } from 'react';

import { useCleanPrevError } from 'hooks';

// Views
import { LoginForm } from 'bus/user/components/login/LoginForm';

import styles from 'bus/user/components/login//styles/index.module.scss';

export const LoginPage: FC = () => {
    useCleanPrevError();

    return (
        <section className = { styles.login }>
            <div className = { styles.content }>
                <h1>Добро пожаловать!</h1>
                <LoginForm />
            </div>
        </section>
    );
};
