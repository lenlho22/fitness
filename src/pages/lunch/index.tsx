import { FC } from 'react';

import { useCleanPrevError } from 'hooks';

import { Base } from 'views/base';

import { QuestionSlector } from 'elements/customQuestionSelector';

const lunch = {
    type:   'lunch',
    title:  'Ты сегодня обедал?',
    inputs: [
        {
            id:    1,
            label: 'Я не обедал',
            value: 'none',
        },
        {
            id:    2,
            label: 'У меня был легкий обед',
            value: 'light',
        },
        {
            id:    3,
            label: 'Я очень плотно покушал',
            value: 'heavy',
        },
    ],
};

export const LunchPage:FC = () => {
    useCleanPrevError();

    return (
        <Base>
            <QuestionSlector { ...lunch } />
        </Base>
    );
};
