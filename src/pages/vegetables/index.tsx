import { FC } from 'react';

import { useCleanPrevError } from 'hooks';

import { Base } from 'views/base';

import { QuestionSlector } from 'elements/customQuestionSelector';

const vegetables = {
    type:   'vegetables',
    title:  'Ты сегодня кушал овощи?',
    inputs: [
        {
            id:    1,
            label: 'Да',
            value: true,
        },
        {
            id:    2,
            label: 'Нет',
            value: false,
        },
    ],
};

export const VegetablesPage:FC = () => {
    useCleanPrevError();

    return (
        <Base>
            <QuestionSlector  { ...vegetables } />
        </Base>
    );
};
