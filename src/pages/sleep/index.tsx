import { FC } from 'react';

import { useCleanPrevError } from 'hooks';

import { Base } from 'views/base';

import { QuestionInput } from 'elements/customQuestionInput';

const sleep = {
    type:        'sleep',
    title:       'Сколько часов ты сегодня спал?',
    placeholder: 'Введите свое число',
};

export const SleepPage:FC = () => {
    useCleanPrevError();

    return (
        <Base>
            <QuestionInput  { ...sleep } />
        </Base>
    );
};
