import { FC } from 'react';

import { useCleanPrevError } from 'hooks';

import { Base } from 'views/base';

import { QuestionCheckboxes } from 'elements/customQuestionCheckboxes';

const water = {
    type:       'water',
    title:      'Сколько воды ты сегодня выпил?',
    inputs:     [...Array(12).keys()].map((i) => ({ id: i + 1, value: i + 1 })),
    multiplier: 250,
};

export const WaterPage:FC = () => {
    useCleanPrevError();

    return (
        <Base>
            <QuestionCheckboxes  { ...water } />
        </Base>
    );
};
