import { FC } from 'react';

import { useCleanPrevError } from 'hooks';
import { Base } from 'views/base';

import { QuestionSlector } from 'elements/customQuestionSelector';

const breakfast = {
    type:   'breakfast',
    title:  'Ты сегодня завтракал?',
    inputs: [
        {
            id:    1,
            label: 'Я не завтракал',
            value: 'none',
        },
        {
            id:    2,
            label: 'У меня был легкий завтрак',
            value: 'light',
        },
        {
            id:    3,
            label: 'Я очень плотно покушал',
            value: 'heavy',
        },
    ],
};

export const BreakfastPage:FC = () => {
    useCleanPrevError();

    return (
        <Base>
            <QuestionSlector { ...breakfast } />
        </Base>
    );
};
