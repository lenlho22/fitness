import { FC } from 'react';

import { useCleanPrevError } from 'hooks';

import { Base } from 'views/base';

import { QuestionSlector } from 'elements/customQuestionSelector';

const fruits = {
    type:   'fruits',
    title:  'Ты сегодня кушал фрукты?',
    inputs: [
        {
            id:    1,
            label: 'Да',
            value: true,
        },
        {
            id:    2,
            label: 'Нет',
            value: false,
        },
    ],
};

export const FruitsPage:FC = () => {
    useCleanPrevError();

    return (
        <Base>
            <QuestionSlector  { ...fruits } />
        </Base>
    );
};
