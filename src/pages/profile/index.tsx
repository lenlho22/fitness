import { FC } from 'react';
import { observer } from 'mobx-react-lite';

import { useStore, useCleanPrevError } from 'hooks';

import { Base } from 'views/base';
import { ProfileForm } from 'bus/user/components/profile';

export const ProfilePage:FC = observer(() => {
    useCleanPrevError();
    const { userStore: { user } } = useStore();

    return (
        <Base disabledWidget center>
            { user ? <ProfileForm { ...user } /> : <></> }
        </Base>
    );
});
