// Core
import { FC } from 'react';

import { useCleanPrevError } from 'hooks';

import { Base } from 'views/base';

import { Dashboard } from 'bus/tracker/components/dashboard';

export const DashboardPage: FC = () => {
    useCleanPrevError();

    return (
        <Base>
            <Dashboard />
        </Base>
    );
};
