import { FC } from 'react';

import { useCleanPrevError } from 'hooks';

import { Base } from 'views/base';

import { QuestionInput } from 'elements/customQuestionInput';

const steps = {
    type:        'steps',
    title:       'Сколько шагов ты сегодня прошел?',
    placeholder: 'Введите свое число',
};

export const StepsPage:FC = () => {
    useCleanPrevError();

    return (
        <Base>
            <QuestionInput  { ...steps } />
        </Base>
    );
};
