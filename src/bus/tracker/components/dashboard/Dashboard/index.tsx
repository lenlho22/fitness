import { FC } from 'react';

import { navItems } from 'constants/dashboard';

import { Item } from './Item';

import styles from '../styles/index.module.scss';

export const Dashboard:FC = () => {
    return (
        <div className = { styles.dashboard }>
            <div className = { styles.navigation }>
                <h1>Как у тебя проходит день?</h1>
                <div className = { styles.items }>
                    { navItems.map((item) => <Item key = { item.index } { ...item } />) }
                </div>
            </div>
        </div>
    );
};
