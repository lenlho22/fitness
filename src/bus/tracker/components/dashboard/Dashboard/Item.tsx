import { FC } from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';

import styles from '../styles/index.module.scss';

interface IItemProps {
    index: string,
    href: string;
    title: string;
    text: string;
}

export const Item:FC<IItemProps> = ({
    index, href, title, text,
}) => {
    return (
        <Link
            className = { cx(styles.link, styles[ `category${index}` ]) }
            to = { href }>
            <span className = { styles.title }>{ title }</span>
            <span className = { styles.description }>{ text }</span>
        </Link>
    );
};
