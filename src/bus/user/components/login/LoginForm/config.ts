import * as yup from 'yup';
import {
    tooShortMessage,
    tooLongMessage,
    wrongEmailMessage,
    requiredEmail,
    PASSWORD_MIN_LENGTH,
    PASSWORD_MAX_LENGTH,
} from 'constants/forms';

export interface ILoginFormShape {
    email: string;
    password: string;
}

export const loginSchema: yup.SchemaOf<ILoginFormShape> = yup.object().shape({
    email: yup
        .string()
        .email(wrongEmailMessage)
        .required(requiredEmail),
    password: yup
        .string()
        .min(PASSWORD_MIN_LENGTH, tooShortMessage)
        .max(PASSWORD_MAX_LENGTH, tooLongMessage)
        .required('*'),
});
