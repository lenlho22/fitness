import { FC } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { observer } from 'mobx-react-lite';

import { useLogin, useStore } from 'hooks';
import { ICredentialsModel } from 'types';
import { ErrorMessage } from 'elements/errorMessage';
import { loginSchema } from './config';

import styles from '../styles/index.module.scss';

export const LoginForm: FC = observer(() => {
    const auth = useLogin();
    const { uiStore: { errorMessage, setErrorMessage } } = useStore();

    const {
        handleSubmit, register, formState,
    } = useForm({
        mode:     'onTouched',
        resolver: yupResolver(loginSchema),
    });

    const login = handleSubmit(async (credentials: ICredentialsModel) => {
        setErrorMessage('');
        const { email, password } = credentials;
        await auth.mutateAsync({ email, password });
    });

    return (
        <form onSubmit = { login }>
            <div className = { styles.inputRow }>
                <label>Электропочта</label>
                <input
                    placeholder = 'Электропочта'
                    type = 'email'
                    { ...register('email') } />
            </div>
            { formState.errors.email
                && <ErrorMessage message = { formState.errors.email.message } /> }
            <div className = { styles.inputRow }>
                <label>Пароль</label>
                <input
                    placeholder = 'Пароль'
                    type = 'password'
                    { ...register('password') } />
            </div>
            { formState.errors.password
                && <ErrorMessage message = { formState.errors.password.message } /> }
            { errorMessage && <ErrorMessage message = { errorMessage } /> }
            <div>
                <button
                    type = 'submit'>
                    Войти в систему
                </button>
                <div className = 'styles_loginLink__1Vm10'>
                    <span>Если у вас нет аккаунта, пожалуйста</span>&nbsp;
                    <Link to = '/registration' >зарегистрируйтесь</Link>.
                </div>
            </div>
        </form>
    );
});
