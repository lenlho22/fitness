import { FC } from 'react';

import { useRemoveAllRecords } from 'hooks';

import styles from '../styles/index.module.scss';

export const RecordsClearButton:FC = () => {
    const remove = useRemoveAllRecords();

    const handleClick = () => {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        remove.mutateAsync();
    };

    return (
        <button
            type = 'button'
            onClick = { handleClick }
            className = { styles.clearAllRecords }>
            Очистить все данные
        </button>
    );
};
