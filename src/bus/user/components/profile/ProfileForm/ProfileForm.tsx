import { FC, Fragment, useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { observer } from 'mobx-react-lite';
import cx from 'classnames';

import { useSignUp, useUpdateUser, useStore } from 'hooks';
import { IProfile, Sex, InputInfoType } from 'types';

import { profileInputs } from 'constants/profile';
import { ErrorMessage } from 'elements/errorMessage';
import { profileSchema } from './config';

import { RecordsClearButton } from '../RecordsClearButton';

import styles from '../styles/index.module.scss';

interface IProfileFormProps extends IProfile {
    registration?: boolean
}

export const ProfileForm: FC<IProfileFormProps>
    = observer(({ children, registration = false, ...defaultValues }) => {
        const [sexInForm, setSexInForm] = useState<string >(defaultValues.sex);
        const auth = useSignUp();
        const update = useUpdateUser();
        const { uiStore: { errorMessage, setErrorMessage } } = useStore();

        const {
            handleSubmit, register, formState, setValue, reset,
        } = useForm({
            mode:     'onTouched',
            resolver: yupResolver(profileSchema),
            defaultValues,
        });

        const submit = handleSubmit(async (values: IProfile) => {
            setErrorMessage('');
            if (registration) {
                await auth.mutateAsync(values);
            } else {
                await update.mutateAsync(values);
            }
        });

        const handleGender = (gender: Sex) => {
            setValue('sex', gender, { shouldDirty: true });
            setSexInForm(gender);
        };

        const handleReset = () => {
            setValue('sex', defaultValues.sex, { shouldDirty: true });
            setSexInForm(defaultValues.sex);
            reset();
        };

        const inputJSX: FC<InputInfoType> = ({
            name, type, label, placeholder,
        }) => (
            <Fragment key = { name.toString() }>
                <div  className = { styles.inputRow }>
                    <label>{ label }</label>
                    { type === 'number' ? (
                        <input
                            placeholder = { placeholder }
                            type = { type }
                            { ...register(name, {
                                valueAsNumber: true,
                            }) } />
                    ) : (
                        <input
                            placeholder = { placeholder }
                            type = { type }
                            { ...register(name) } />
                    ) }
                </div>
                { formState.errors && formState.errors[ name ]
                    && <ErrorMessage message = { formState.errors[ name ]?.message || '' } /> }
            </Fragment>
        );

        return (
            <div className = { styles.profile }>
                <h1>Профиль</h1>
                <form onSubmit = { submit }>

                    <div className = { styles.gender }>
                        <input
                            type = 'radio'
                            value = { Sex.FEMALE }
                            style = { { display: 'none' } }
                            { ...register('sex') } />
                        <input
                            type = 'radio'
                            value = { Sex.MALE }
                            style = { { display: 'none' } }
                            { ...register('sex') } />
                        <div
                            className = { cx(styles.female, {
                                [ styles.selected ]: sexInForm === Sex.FEMALE,
                            }) }
                            onClick = { () => handleGender(Sex.FEMALE) }>
                            <span>Женщина</span>
                        </div>
                        <div
                            className = { cx(styles.male, {
                                [ styles.selected ]: sexInForm === Sex.MALE,
                            }) }
                            onClick = { () => handleGender(Sex.MALE) }>
                            <span>Мужчина</span>
                        </div>
                    </div>
                    { !sexInForm && formState.errors.sex && formState.errors.sex.message
                        && <ErrorMessage message = { formState.errors.sex.message } /> }
                    { profileInputs
                        .map((inputInfo: InputInfoType) => inputJSX(inputInfo)) }

                    { errorMessage && <ErrorMessage message = { errorMessage } /> }

                    <div className = { styles.controls }>
                        <button
                            type = 'button' className = { styles.clearData }
                            onClick = { handleReset }>
                            Сбросить
                        </button>
                        <button
                            type = 'submit'>
                            { registration ? 'Зарегистрироваться' : 'Обновить' }
                        </button>
                    </div>
                </form>

                { !registration && <RecordsClearButton /> }
            </div>
        );
    });
