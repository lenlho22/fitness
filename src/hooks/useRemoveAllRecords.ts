/* Core */
import { useMutation, UseMutationResult } from 'react-query';

/* Other */
import { api } from 'api';
import { useStore } from 'hooks';
import { queryClient } from 'lib/react-query';
import { ErrorType } from 'types';

export const useRemoveAllRecords = ():UseMutationResult<unknown, ErrorType, void> => {
    const { userStore: { token }, uiStore: { setErrorMessage } } = useStore();

    const mutation = useMutation<unknown, ErrorType, void>(() => {
        return api.tracker.removeAllRecords(token);
    },
    {
        onError: (error: ErrorType) => {
            const { message } = error.response.data;
            setErrorMessage(message);
        },
        onSuccess: () => queryClient.invalidateQueries('score'),
    });

    return mutation;
};
