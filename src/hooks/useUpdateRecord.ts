/* Core */
import { useMutation, UseMutationResult } from 'react-query';

/* Other */
import { api } from 'api';
import { useStore } from 'hooks';
import { queryClient } from 'lib/react-query';
import { IRecord, ErrorType, IResponseRecord } from 'types';

type VariablesType = {
    record: IRecord,
    hash: string,
};

export const useUpdateRecord
    = (): UseMutationResult<IResponseRecord, ErrorType, VariablesType> => {
        const { userStore: { token }, uiStore: { setErrorMessage } } = useStore();

        const mutation
        = useMutation<IResponseRecord, ErrorType, VariablesType>(({ record, hash }) => {
            return api.tracker.updateRecord(record, hash, token);
        },
        {
            onSuccess: () => queryClient.invalidateQueries('score'),
            onError:   (error: ErrorType) => {
                const { message } = error.response.data;
                setErrorMessage(message);
            },
        });

        return mutation;
    };
