import { useEffect } from 'react';

import { useStore } from 'hooks';

export const useCleanPrevError = () => {
    const { uiStore: { setErrorMessage } } = useStore();

    useEffect(() => {
        setErrorMessage('');
    }, []);
};
