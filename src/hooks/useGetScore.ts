/* Core */
import { useQuery, UseQueryResult } from 'react-query';

/* Other */
import { api } from 'api';
import { useStore } from 'hooks';
import { ErrorType } from 'types';

export const useGetScore = (): UseQueryResult<number, ErrorType> => {
    const { userStore: { token }, uiStore: { setErrorMessage } } = useStore();

    const query = useQuery<number, ErrorType>(
        ['score', token],
        () => api.tracker.getScore(token),
        {
            enabled: !!token,
            onError: (error: ErrorType) => {
                const { message } = error.response.data;
                setErrorMessage(message);
            },
        },
    );

    return query;
};
