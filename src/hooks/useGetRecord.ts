/* Core */
import { useQuery, UseQueryResult } from 'react-query';

/* Other */
import { api } from 'api';
import { useStore } from 'hooks';
import { IResponseRecord, ErrorType } from 'types';

export const useGetRecord
= (kind: string):UseQueryResult<IResponseRecord, ErrorType> => {
    const { userStore: { token }, uiStore: { setErrorMessage } } = useStore();

    const query = useQuery<IResponseRecord, ErrorType>(

        ['record', token],
        () => api.tracker.getRecord(kind, token),
        {
            cacheTime: 0,
            enabled:   !!token,
            onError:   (error: ErrorType) => {
                const { message } = error.response.data;
                setErrorMessage(message);
            },
        },
    );

    return query;
};
