import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import { TOKEN_KEY } from 'constants/auth';
import { useStore } from 'hooks';

interface IUseAuth {
    logout: () => void;
}

export const useAuth = (): IUseAuth => {
    const { userStore: { token, setToken, setUser } } = useStore();
    const navigate = useNavigate();

    useEffect(() => {
        const localToken = localStorage.getItem(TOKEN_KEY);

        if (localToken && !token) {
            setToken(localToken);

            return;
        }

        if (!localToken && !token) {
            navigate('/login');
        }
    }, [token]);

    return {
        logout: () => {
            localStorage.removeItem(TOKEN_KEY);
            navigate('/login');
            setToken(null);
            setUser(null);
        },
    };
};
