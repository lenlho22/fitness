/* Core */
import { useMutation, UseMutationResult } from 'react-query';

/* Other */
import { api } from 'api';
import { queryClient } from 'lib/react-query';
import { IProfile, ErrorType } from 'types';
import { useStore } from './useStore';

const signupErrorMessage =  'Попробуйте пройти регистрацию ещё раз!';

export const useUpdateUser = (): UseMutationResult<IProfile, ErrorType, IProfile> => {
    const { userStore, uiStore: { setErrorMessage } } = useStore();
    const { token } = userStore;

    const mutation = useMutation<IProfile, ErrorType, IProfile>((payload: IProfile) => {
        return api.users.updateMe(payload, token);
    },
    {
        onError: (error: ErrorType) => {
            const { message } = error.response.data;
            const errorMessage = `
            ${signupErrorMessage}:\n
            ${message}
            `;
            setErrorMessage(errorMessage);
        },
        onSuccess: (data) => {
            // @ts-ignore
            const { hash, ...newUser } = data;
            userStore.setUser(newUser);

            // eslint-disable-next-line @typescript-eslint/no-floating-promises
            queryClient.invalidateQueries('user');
        },
    });

    return mutation;
};
