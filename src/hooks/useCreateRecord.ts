/* Core */
import { useMutation, UseMutationResult } from 'react-query';

/* Other */
import { api } from 'api';
import { useStore } from 'hooks';
import { queryClient } from 'lib/react-query';
import {  IRecord, ErrorType } from 'types';

export const useCreateRecord
    = (): UseMutationResult<IRecord, ErrorType, IRecord> => {
        const { userStore: { token }, uiStore: { setErrorMessage } } = useStore();

        const mutation = useMutation<IRecord, ErrorType, IRecord>((newRecord: IRecord) => {
            return api.tracker.createRecord(newRecord, token);
        },
        {
            onError: (error: ErrorType) => {
                const { message } = error.response.data;
                setErrorMessage(message);
            },
            onSuccess: () => queryClient.invalidateQueries('score'),
        });

        return mutation;
    };
