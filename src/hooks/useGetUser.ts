/* Core */
import { useQuery, UseQueryResult } from 'react-query';

/* Other */
import { api } from 'api';
import { useStore } from 'hooks';
import { IProfile, ErrorType } from 'types';

export const useGetUser = (): UseQueryResult<IProfile, ErrorType> => {
    const { userStore: { token }, uiStore: { setErrorMessage } } = useStore();

    const query = useQuery<IProfile, ErrorType>(
        ['user', token],
        () => api.users.getMe(token),
        {
            enabled: !!token,
            onError: (error: ErrorType) => {
                const { message } = error.response.data;
                setErrorMessage(message);
            },
        },
    );

    return query;
};
