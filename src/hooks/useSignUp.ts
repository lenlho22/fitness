/* Core */
import { useMutation, UseMutationResult } from 'react-query';
import { useNavigate } from 'react-router-dom';

/* Other */
import { api } from 'api';
import { IProfile, ErrorType } from 'types';
import { TOKEN_KEY } from 'constants/auth';
import { useStore } from './useStore';

const signupErrorMessage =  'Попробуйте пройти регистрацию ещё раз!';

export const useSignUp = (): UseMutationResult<string, ErrorType, IProfile> => {
    const { userStore, uiStore: { setErrorMessage } } = useStore();
    const navigate = useNavigate();

    const mutation = useMutation<string, ErrorType, IProfile>((payload) => {
        return api.users.create(payload);
    },
    {
        onError: (error: ErrorType) => {
            const { message } = error.response.data;
            const errorMessage = `
            ${signupErrorMessage}:\n
            ${message}
            `;
            setErrorMessage(errorMessage);
        },
        onSuccess: (data) => {
            const  token   = data;
            userStore.setToken(token);
            localStorage.setItem(TOKEN_KEY, token);
            navigate('/');
        },
    });

    return mutation;
};
