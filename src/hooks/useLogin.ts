/* Core */
import { useMutation, UseMutationResult } from 'react-query';
import { useNavigate } from 'react-router-dom';

/* Other */
import { api } from 'api';
import { ErrorType } from 'types';
import { TOKEN_KEY } from 'constants/auth';
import { useStore } from './useStore';

const loginErrorMessage =  'Попробуйте пройти авторизацию ещё раз!';

type VariablesType = {
    email: string,
    password: string,
};

export const useLogin = (): UseMutationResult<string, ErrorType, VariablesType> => {
    const { userStore, uiStore: { setErrorMessage } } = useStore();
    const navigate = useNavigate();

    const mutation = useMutation<string, ErrorType, VariablesType>(({ email, password }) => {
        return api.users.login(btoa(`${email}:${password}`));
    },
    {
        onError: (error: ErrorType) => {
            const { message } = error.response.data;
            const errorMessage = `
            ${loginErrorMessage}:\n
            ${message}
            `;
            setErrorMessage(errorMessage);
        },
        onSuccess: (data) => {
            const  token   = data;
            userStore.setToken(token);
            localStorage.setItem(TOKEN_KEY, token);
            navigate('/');
        },
    });

    return mutation;
};
